# rocket-hello

A [Docker](https://www.docker.com/) `FROM scratch` [Rocket](https://rocket.rs) Hello World.

```
make run-docker
```
