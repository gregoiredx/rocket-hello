run:
	cargo run
watch:
	cargo watch -x run
build:
	rustup target add x86_64-unknown-linux-musl
	cargo build --target=x86_64-unknown-linux-musl --release
build-docker: build
	docker image rm rocket-hello --force
	docker build . -t rocket-hello:latest
run-docker: build-docker
	docker run -p 8000:8000 -ti rocket-hello:latest
