FROM scratch
ADD target/x86_64-unknown-linux-musl/release/rocket-hello /
ADD Rocket.toml /
CMD ["/rocket-hello"]
